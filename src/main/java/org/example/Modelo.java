package org.example;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.util.ArrayList;

public class Modelo {
    private String prefijoConexion = "jdbc:mysql://";
    private String driver = "com.mysql.cj.jdbc.Driver";

    private String bd = "carmen_sandiego";
    private String ip = "localhost";
    private String usr = "";
    private String psw = "";
    private Connection connection;
    private ActionListener listener;

    public Modelo() {
        listener = null;

        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            reportException(e.getMessage());
        }

    }
    public String primerCiudad() throws SQLException {
        String resultado = "";
        connection = obtenerConexion();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT origen FROM carmen_sandiego.ciudades LIMIT 1;");
        while (resultSet.next()) {
            resultado = resultSet.getString(1);
        }
        resultSet.close();
        statement.close();
        return resultado;
    }
    public ArrayList<String> consultarCiudad(String ciudad) throws SQLException {
        ArrayList resultados = new ArrayList<String>();
        connection = obtenerConexion();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT destino1,destino2,destino3 FROM carmen_sandiego.ciudades WHERE origen=\""+ciudad+"\";");
        while (resultSet.next()) {
            resultados.add(resultSet.getString(1));
            resultados.add(resultSet.getString(2));
            resultados.add(resultSet.getString(3));
        }
        resultSet.close();
        statement.close();
        return resultados;
    }






    public void addExceptionListener(ActionListener listener) {
        this.listener = listener;
    }
    private void reportException(String exception) {
        if (listener != null) {
            ActionEvent evt = new ActionEvent(this, 0, exception);
            listener.actionPerformed(evt);
        }
    }
    private Connection obtenerConexion() {
        if (connection == null) {
            try {
                Class.forName(driver); // driver = "com.mysql.cj.jdbc.Driver";
            } catch (ClassNotFoundException ex) {
                reportException(ex.getMessage());
            }
            try { // prefijoConexion = "jdbc:mysql://";
                connection =
                        DriverManager.getConnection(prefijoConexion + ip + "/" + bd, usr, psw);
            } catch (Exception ex) {
                reportException(ex.getMessage());
            }
            Runtime.getRuntime().addShutdownHook(new ShutDownHook());
        }
        return connection;
    }
    private class ShutDownHook extends Thread {
        public void run() {
            try {
                if (connection != null) connection.close();
            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
        }
    }

}
