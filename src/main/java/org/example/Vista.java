package org.example;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

public class Vista {
    Modelo m;
    private JFrame f, p;
    private JLabel titulo, cActual, posDestinos, msjVictoria;
    private JTextField txtActual, txtCiudad1, txtCiudad2, txtCiudad3;
    private JButton irCiudad1, irCiudad2, irCiudad3, ok;

    public Vista(Modelo m) throws SQLException {
        this.m = m;
        f = new JFrame("¿Dónde en el mundo está Carmen Sandiego?");
        titulo = new JLabel("¿Dónde en el mundo está Carmen Sandiego?");
        cActual = new JLabel("Ciudad Actual: ");
        posDestinos = new JLabel("Posibles Destinos:");
        msjVictoria = new JLabel("");
        String primerCiudad = m.primerCiudad();
        txtActual = new JTextField(primerCiudad ,20);
        ArrayList<String> ciudades = m.consultarCiudad(primerCiudad);

        txtCiudad1 = new JTextField(ciudades.get(0),20);
        txtCiudad2 = new JTextField(ciudades.get(1),20);
        txtCiudad3 = new JTextField(ciudades.get(2),20);
        irCiudad1 = new JButton("Ir");
        irCiudad2 = new JButton("Ir");
        irCiudad3 = new JButton("Ir");
        ok = new JButton("OK");



        f.getContentPane().setBackground(Color.CYAN);
        f.getContentPane().setLayout(new FlowLayout(FlowLayout.LEFT));

        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        f.setLocationRelativeTo(null);

        f.getContentPane().add(titulo);
        f.getContentPane().add(cActual);
        f.getContentPane().add(txtActual);
        f.getContentPane().add(posDestinos);
        f.getContentPane().add(txtCiudad1);
        f.getContentPane().add(irCiudad1);
        f.getContentPane().add(txtCiudad2);
        f.getContentPane().add(irCiudad2);
        f.getContentPane().add(txtCiudad3);
        f.getContentPane().add(irCiudad3);
        f.setSize(300,250);
        f.setVisible(true);




    }

    public void setCiudades(String actual, ArrayList<String> ciudades){
        txtActual.setText(actual);
        txtCiudad1.setText(ciudades.get(0));
        txtCiudad2.setText(ciudades.get(1));
        txtCiudad3.setText(ciudades.get(2));
    }

    public String getCiudad1(){
        return txtCiudad1.getText();
    }
    public String getCiudad2(){
        return txtCiudad2.getText();
    }
    public String getCiudad3(){
        return txtCiudad3.getText();
    }
    public void botonIr1(ActionListener av) {
        irCiudad1.addActionListener(av);
    }
    public void botonIr2(ActionListener av) {
        irCiudad2.addActionListener(av);
    }
    public void botonIr3(ActionListener av) {
        irCiudad3.addActionListener(av);
    }
    public void ok(ActionListener av) {
        ok.addActionListener(av);
    }
    public void victoria(int v){
        JOptionPane.showMessageDialog(f,
                "¡Encontraste a Carmen Sandiego en "+v+" viajes",
                "Ganaste",
                JOptionPane.DEFAULT_OPTION);
        System.exit(0);

    }

}
