package org.example;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class Controlador {
    private Modelo m;
    private Vista v;

    private int cantViajes;

    public Controlador(Modelo m, Vista v){
        this.m = m;
        this.v = v;
        cantViajes = 0;
    }


    public void ejecutar(){
        v.botonIr1(new ir1Listener());
        v.botonIr2(new ir2Listener());
        v.botonIr3(new ir3Listener());
        v.ok(new okListener());
    }
    private class ir1Listener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            cantViajes++;
            String actual = v.getCiudad1();
            try {

                v.setCiudades(actual,m.consultarCiudad(actual));
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            if(actual.equalsIgnoreCase("Lagos")){
                v.victoria(cantViajes);

            }
        }
    }
    private class ir2Listener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            cantViajes++;
            String actual = v.getCiudad2();
            try {

                v.setCiudades(actual,m.consultarCiudad(actual));
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            if(actual.equalsIgnoreCase("Lagos")){
                v.victoria(cantViajes);

            }
        }
    }
    private class ir3Listener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            cantViajes++;
            String actual = v.getCiudad3();

            try {

                v.setCiudades(actual,m.consultarCiudad(actual));
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            if(actual.equalsIgnoreCase("Lagos")){
                v.victoria(cantViajes);

            }
        }
    }
    private class okListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {

        }
    }

}
